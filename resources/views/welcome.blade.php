@extends('layout.main')

@section('content')

    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
            <form action="" id="process_from">
                <div class="white-box">
                    <h3 class="box-title">From</h3>
                    <div class="d-md-flex">
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <dic class="form-group">
                                <label for="">Customer Data <small class="alert-primary">Format NAME[SPACE]AGE[SPACE]CITY[SPACE]</small></label>
                                <input type="text" class="form-control" id="customer_data">
                            </dic>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-2">
                            <button id="btn-process" class="btn btn-primary">Process</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection


@section('script')
    <script>
        $( document ).ready(function() {
            $("#process_from").on('submit',function (e){
                e.preventDefault(e);
                let customerData = $("#customer_data").val();
                $('#loading-spinner').modal('show')
                $.ajax({
                    url : 'http://localhost:8080/api/data/save',
                    method : 'POST',
                    data : {customerData:customerData},
                    success: function (data){
                        console.log("process_from",data);

                        setTimeout(function () {
                            $('#loading-spinner').modal('hide');
                        }, 1000);

                        if (data.success){
                            Swal.fire(
                                'Data Successfuly submited',
                                data.message,
                                'success'
                            )
                        }else{
                            Swal.fire(
                                'Failed Input Data',
                                data.message,
                                'error'
                            )
                        }
                    },error:function (){
                        Swal.fire(
                            'Faile Input Data',
                            "Unknown error",
                            'error'
                        )
                    }
                })

            });
        });
    </script>
@endsection
