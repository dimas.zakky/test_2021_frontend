<?php


namespace App\Services;


use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;

class ApiService
{
    protected $BASE_URL;
    public function __construct()
    {
        $this->BASE_URL = env('BASE_URL');
    }

    public function get($url) {
        $token = Session::get('_token');
        $response = Http::withToken($token)->get($this->BASE_URL.$url);

        return $response;
    }

    public function post($url,$data) {
        $token = Session::get('_token');
        $response = Http::withToken($token)->post($this->BASE_URL.$url,$data);

        return $response;
    }

}
