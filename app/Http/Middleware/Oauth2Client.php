<?php

namespace App\Http\Middleware;

use App\Entity\MUser;
use App\User;
use Closure;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;
use Auth;
class Oauth2Client
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (empty($request->bearerToken())) {
            return response()->json(
                [
                    'error' => 'no token provided'
                ], 401);
        } else {
            $response = $this->checkToken($request->bearerToken());
            if ($response->status() == 200) {
                $res_body = json_decode(json_encode(simplexml_load_string($response->body())), true);
                $user = new MUser($res_body);
                Auth::login($user);
            } elseif ($response->status() == 400) {
                return response()->json(
                    [
                        json_decode($response->body())
                    ], 400);
            } elseif ($response->status() == 401) {
                return response()->json(
                    [
                        "error"=> "invalid_token",
                        "error_description"=>"Invalid access token: ".$request->bearerToken()
                    ], 401);
            }
        }
        return $next($request);
    }

    function checkToken($token)
    {
        $response = Http::withToken($token)->post(env('CHECK_TOKEN_URL'));
        Log::info('=$response=' . $response->status() . '==body==' . json_encode($response->body()));
        return $response;
    }
}
