<?php

namespace App\Http\Middleware;

use App\Entity\MUser;
use App\Models\User;
use Closure;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;
use Auth;
class Oauth2ClientWeb
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->session()->has('_token')){
            $response = $this->checkToken($request->session()->get('_token'));

            if ($response->status() == '401'){
                return redirect('/login')->withErrors(['msg' => $response->json()['message']]);
            }

            if ($response->status() == '200'){
                $u = $response->json()['result'];
                $user = new User();
                $user->id = $u['id'];
                $user->nama = $u['fullname'];
                $user->level_name = $u['level_name'];
                Auth::login($user);
            }else{
                return redirect('/login')->withErrors(['msg' => 'Login gagal']);
            }
        }else{
            return redirect('/login')->withErrors(['msg' => 'Anda belum login']);
        }
        return $next($request);
    }

    function checkToken($token)
    {
        Log::info('base url ==== '.env('BASE_URL').'/get-user');
        $response = Http::withToken($token)->get(env('BASE_URL').'/get-user');
        Log::info('=$response=' . $response->status() . '==body==' . json_encode($response->body()));
        return $response;
    }
}
