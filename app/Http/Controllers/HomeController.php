<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    //
    public function home(){
        return view('welcome')->with([
            'user'=>Auth::User(),
            'page_title'=>'Dashboard'
        ]);
    }
}
